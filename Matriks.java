import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Matriks {
	
	public static void Menu(){
		System.out.println("Masukkan Pilihan Anda");
		System.out.println("1. Perkalian Matriks");
		System.out.println("2. Penjumlahan Matriks");
		System.out.println("3. Determinan Matriks");
		System.out.println("0. Exit");
	}

	public static void main(String[] args) {
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputData = null;
        int choice = 0;
        RumusMatriks hasil = new RumusMatriks();
        do {
            Menu();
            System.out.println("Masukkan Pilihan Anda: ");
            try {
                inputData = bufferedReader.readLine();
                try  {
                    choice = Integer.parseInt(inputData);
                    if (choice > 0 && choice == 1) {
                        hasil.PerkalianMatriks();
                    }
                    else if (choice > 0 && choice == 2) {
                        hasil.PenjumlahanMatriks();
                    }
                    else if (choice > 0 && choice == 3) {
                        hasil.DeterminanMatriks();
                    }
                    else {
                        System.out.println("Thankyou ^_^");
                    }
                }
                catch(NumberFormatException e) {
                    System.out.println("Masukan Tidak Sesuai");
                }
            }
            catch (IOException error) {
                System.out.println("Error Input " + error.getMessage());
            }
            System.out.println(" ");

        } while(choice > 0);
	

	}

}
