import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class BangunRuang {
	
	public static void Menu() {
        System.out.println("Menu Rumus");
        System.out.println("1. Persegi");
        System.out.println("2. Persegi Panjang");
        System.out.println("3. Segitiga Siku - Siku");
        System.out.println("4. Layang-layang");
        System.out.println("5. Belah Ketupat");
        System.out.println("6. Trapesium");
        System.out.println("7. Jajargenjang");
        System.out.println("8. Kubus");
        System.out.println("9. Balok");
        System.out.println("10.Limas");
        System.out.println("11.Prisma");
        System.out.println("12.Tabung");
        System.out.println("13.Kerucut");
        System.out.println("14.Bola");
        System.out.println("0. Keluar");
 }
 
 

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputData = null;
        int choice = 0;
        RumusBangunRuang hitung = new  RumusBangunRuang();
        do {
            Menu();
            System.out.println("Masukkan Pilihan Anda: ");
            try {
                inputData = bufferedReader.readLine();
                try  {
                    choice = Integer.parseInt(inputData);
                    if (choice > 0 && choice == 1) {
                        hitung.squareFormula();
                    }
                    else if (choice > 0 && choice == 2) {
                        hitung.rectangleFormula();
                    }
                    else if (choice > 0 && choice == 3) {
                        hitung.triangleFormula();
                    }
                    else if (choice > 0 && choice == 4) {
                    	 hitung.LayanglayangFormula();
                    }
                    else if (choice > 0 && choice == 5) {
                    	 hitung.RhombusFormula();
                    }
                    else if (choice > 0 && choice == 6) {
                    	 hitung.TrapezoidFormula();
                    }
                    else if (choice > 0 && choice == 7) {
                    	 hitung.ParallelogramFormula();
                    }
                    else if (choice > 0 && choice == 8) {
                    	 hitung.CubeFormula();
                    }
                    else if (choice > 0 && choice == 9) {
                    	 hitung.CuboidFormula();
                    }
                    else if (choice > 0 && choice == 10){
                    	 hitung.PyramidFormula();
                    }
                    else if (choice > 0 && choice == 11){
                    	 hitung.PrismFormula();
                    }
                    else if (choice > 0 && choice == 12){
                    	 hitung.CylinderFormula();
                    }
                    else if (choice > 0 && choice == 13){
                    	 hitung.ConeFormula();
                    }
                    else if (choice > 0 && choice == 14){
                    	 hitung.SphereFormula();
                    }
                    else {
                        System.out.println("Thankyou ^_^");
                    }
                }
                catch(NumberFormatException e) {
                    System.out.println("Masukan Tidak Sesuai");
                }
            }
            catch (IOException error) {
                System.out.println("Error Input " + error.getMessage());
            }

        } while(choice > 0);

	}

}
