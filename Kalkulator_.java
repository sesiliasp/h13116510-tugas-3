import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Kalkulator_ {
	
	static void Menu(){
		System.out.println("Masukkan Pilihan Anda");
		System.out.println("1. Perkalian ");
		System.out.println("2. Penjumlahan ");
		System.out.println("3. Pengurangan");
		System.out.println("4. Pembagian");
	}
	
	static void tambah(){
		System.out.println("Masukan Nilai Pertama : ");
	    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	    String inputDataNilai1 = null;
	    try {
	        inputDataNilai1 = bufferedReader.readLine();
	    }
	    catch (IOException error) {
	        System.out.println("Error Input " + error.getMessage());
	    }

	    System.out.println("Masukan Nilai Kedua: ");
	    bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	    String inputDataNilai2 = null;
	    try {
	        inputDataNilai2 = bufferedReader.readLine();
	    }
	    catch (IOException error) {
	        System.out.println("Error Input " + error.getMessage());
	    }

	    try  {
	        int Nilai1 = Integer.parseInt(inputDataNilai1);
	        int Nilai2 = Integer.parseInt(inputDataNilai1);
	        int hasil = Nilai1 + Nilai2;
	        System.out.println("Hasil Penjumlahannya adalah: " + hasil);
	        
	    }
	    catch(NumberFormatException e) {
	        System.out.println("Masukan Tidak Sesuai");
	    	}
	}
	
	static void kurang(){
		System.out.println("Masukan Nilai Pertama : ");
	    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	    String inputDataNilai1 = null;
	    try {
	        inputDataNilai1 = bufferedReader.readLine();
	    }
	    catch (IOException error) {
	        System.out.println("Error Input " + error.getMessage());
	    }

	    System.out.println("Masukan Nilai Kedua: ");
	    bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	    String inputDataNilai2 = null;
	    try {
	        inputDataNilai2 = bufferedReader.readLine();
	    }
	    catch (IOException error) {
	        System.out.println("Error Input " + error.getMessage());
	    }

	    try  {
	        int Nilai1 = Integer.parseInt(inputDataNilai1);
	        int Nilai2 = Integer.parseInt(inputDataNilai1);
	        int hasil = Nilai1 - Nilai2;
	        System.out.println("Hasil Pengurangannya adalah: " + hasil);
	        
	    }
	    catch(NumberFormatException e) {
	        System.out.println("Masukan Tidak Sesuai");
	    	}
	}
	
	static void kali(){
		System.out.println("Masukan Nilai Pertama : ");
	    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	    String inputDataNilai1 = null;
	    try {
	        inputDataNilai1 = bufferedReader.readLine();
	    }
	    catch (IOException error) {
	        System.out.println("Error Input " + error.getMessage());
	    }

	    System.out.println("Masukan Nilai Kedua: ");
	    bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	    String inputDataNilai2 = null;
	    try {
	        inputDataNilai2 = bufferedReader.readLine();
	    }
	    catch (IOException error) {
	        System.out.println("Error Input " + error.getMessage());
	    }

	    try  {
	        int Nilai1 = Integer.parseInt(inputDataNilai1);
	        int Nilai2 = Integer.parseInt(inputDataNilai1);
	        int hasil = Nilai1 * Nilai2;
	        System.out.println("Hasil Perkaliannya adalah: " + hasil);
	        
	    }
	    catch(NumberFormatException e) {
	        System.out.println("Masukan Tidak Sesuai");
	    	}
	}
	static void bagi(){
		System.out.println("Masukan Nilai Pertama : ");
	    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	    String inputDataNilai1 = null;
	    try {
	        inputDataNilai1 = bufferedReader.readLine();
	    }
	    catch (IOException error) {
	        System.out.println("Error Input " + error.getMessage());
	    }

	    System.out.println("Masukan Nilai Kedua: ");
	    bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	    String inputDataNilai2 = null;
	    try {
	        inputDataNilai2 = bufferedReader.readLine();
	    }
	    catch (IOException error) {
	        System.out.println("Error Input " + error.getMessage());
	    }

	    try  {
	        int Nilai1 = Integer.parseInt(inputDataNilai1);
	        int Nilai2 = Integer.parseInt(inputDataNilai1);
	        int hasil = Nilai1 / Nilai2;
	        System.out.println("Hasil Pembagiannya adalah: " + hasil);
	        
	    }
	    catch(NumberFormatException e) {
	        System.out.println("Masukan Tidak Sesuai");
	    	}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputData = null;
        int choice = 0;
        Kalkulator_ hitung = new Kalkulator_();
        do {
            Menu();
            System.out.println("Masukkan Pilihan: ");
            try {
                inputData = bufferedReader.readLine();
                try  {
                    choice = Integer.parseInt(inputData);
                    if (choice > 0 && choice == 1) {
                        hitung.kali();
                    }
                    else if (choice > 0 && choice == 2) {
                    	 hitung.tambah();
                    }
                    else if (choice > 0 && choice == 3) {
                    	 hitung.kurang();
                    }
                    else if (choice > 0 && choice == 4) {
                    	hitung.bagi();
                    }
                    else {
                        System.out.println("Terima Kasih");
                    }
                }
                catch(NumberFormatException e) {
                    System.out.println("Masukan Tidak Sesuai");
                }
            }
            catch (IOException error) {
                System.out.println("Error Input " + error.getMessage());
            }
            System.out.println(" ");

        } while(choice > 0);
		
		

	}

}
